package com.brilliantsoft.bravo.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.brilliantsoft.bravo.accounts.Account;
import com.brilliantsoft.bravo.accounts.AccountRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	private AccountRepository userRepository;
	
	@Autowired
	public CustomUserDetailsService(AccountRepository userRepository) {
		this.userRepository = userRepository;
	}
	
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    	Account user = userRepository.findByUsername(username);
    	if (user == null) {
    		throw new UsernameNotFoundException("Username " + username + " was not found in the database");
    	}
    		
    	return new CustomUserDetails(user);
    }

}