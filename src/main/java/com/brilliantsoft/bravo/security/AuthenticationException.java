package com.brilliantsoft.bravo.security;

import com.brilliantsoft.bravo.exceptions.BaseException;

public class AuthenticationException extends BaseException {

	private static final long serialVersionUID = 1L;

	public AuthenticationException(String code) {
		super("Incorrect username or password", code);
	}
}