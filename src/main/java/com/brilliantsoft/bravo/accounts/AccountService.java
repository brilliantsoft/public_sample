package com.brilliantsoft.bravo.accounts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.brilliantsoft.bravo.dto.Credentials;
import com.brilliantsoft.bravo.dto.UserToken;
import com.brilliantsoft.bravo.exceptions.InvalidCredentialsException;
import com.brilliantsoft.bravo.exceptions.ExceptionConstants.ErrorCodes;

@Service
public class AccountService {

	private AccountRepository accountRepository;
	private RestTemplate restTemplate;

	@Value("${system.web.credentials.base64}")
	private String base64Credentials;
	
	@Autowired
	public AccountService(AccountRepository accountRepository, RestTemplateBuilder restTemplateBuilder) {
		this.accountRepository = accountRepository;
		this.restTemplate = restTemplateBuilder.build();
	}
	
	public Account findAccount(String username) {
		return accountRepository.findByUsername(username);
	}

	public UserToken authenticate(Credentials credentials) {
		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
	    headers.add("Authorization", "Basic " + base64Credentials);
	    
	    MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
	    map.add("grant_type", "password");
	    map.add("username", credentials.getUsername());
	    map.add("password", credentials.getPassword());

	    HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<MultiValueMap<String, String>>(map, headers);
	    ResponseEntity<UserToken> result = null;
	    try {
	    	result = restTemplate.exchange("http://localhost:8080/oauth/token", 
	    		HttpMethod.POST, entity, UserToken.class);
	    } catch (HttpClientErrorException ex) {
	    	throw new InvalidCredentialsException(ErrorCodes.INVALID_CREDENTIALS);
	    }
		return result.getBody();
	}
}