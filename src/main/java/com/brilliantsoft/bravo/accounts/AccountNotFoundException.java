package com.brilliantsoft.bravo.accounts;

import org.springframework.security.core.AuthenticationException;

public class AccountNotFoundException extends AuthenticationException {

	private static final long serialVersionUID = 1L;

	//~ Constructors ===================================================================================================

    /**
     * Constructs a <code>UserNotFoundException</code> with the specified
     * message.
     *
     * @param msg the detail message.
     */
    public AccountNotFoundException(String msg) {
        super(msg);
    }

    /**
     * Constructs a {@code UserNotFoundException} with the specified message and root cause.
     *
     * @param msg the detail message.
     * @param t root cause
     */
    public AccountNotFoundException(String msg, Throwable t) {
        super(msg, t);
    }
}