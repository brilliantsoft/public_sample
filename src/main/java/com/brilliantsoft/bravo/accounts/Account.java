package com.brilliantsoft.bravo.accounts;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.JoinColumn;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.brilliantsoft.bravo.data.BaseEntity;
import com.brilliantsoft.bravo.events.Event;
import com.brilliantsoft.bravo.exceptions.EventNotFoundException;
import com.brilliantsoft.bravo.exceptions.ExceptionConstants.ErrorCodes;

@Entity
@Table(name = "account")
public class Account extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
	
	@Column(name="firstname")
    private String firstName;
	
	@Column(name="lastname")
    private String lastName;
	
	@Column(name="username", unique=true)
	private String username;

	@Column(name="password")
    private String password;
	
	@Column(name="email")
    private String email;

	@Column(name="phonenumber")
    private String phoneNumber;
    
    @Column(name="isverified")
	private Boolean isVerified;

	@Column(name="dateofbirthmonth")
    private Integer dateofbirthmonth;

	@Column(name="dateofbirthyear")
    private Integer dateofbirthyear;

	@Column(name="dateofbirthday")
    private Integer dateofbirthday;

	@Column(name="blacklisted")
    private Boolean blacklisted;

	@Column(name="locale")
    private String locale;

	@Column(name="hasavatar")
    private Boolean hasAvatar;
	
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinTable(name = "accountrole", 
		       joinColumns = { @JoinColumn(name = "accountid", nullable = false, updatable = false) }, 
			   inverseJoinColumns = { @JoinColumn(name = "roleid", nullable = false, updatable = false) })
	private Set<Role> roles = new HashSet<Role>(0);
	
    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.TRUE)
	private Set<Event> events = new HashSet<Event>();
    
    public Account() {
    	super();
		this.setActive(true);
		this.setDeleted(false);
    }
    
    public Account(String email, String password, Set<Role> roles) {
		super();
		this.email = email;
		this.password = password;
		this.roles = roles;
		this.setActive(true);
		this.setDeleted(false);
	}

	public Account(AccountBuilder userBuilder) {
		super();
		this.setActive(true);
		this.setDeleted(false);
		this.email = userBuilder.email;
		this.firstName = userBuilder.firstName;
		this.lastName = userBuilder.lastName;
		this.phoneNumber = userBuilder.phoneNumber;
		this.password = userBuilder.password;
		this.isVerified = userBuilder.isVerified;
		this.username = userBuilder.username;
		this.dateofbirthmonth = userBuilder.dateofbirthmonth;
		this.dateofbirthyear = userBuilder.dateofbirthyear;
		this.dateofbirthday = userBuilder.dateofbirthday;
		if (userBuilder.roles != null) {
			this.roles = userBuilder.roles;
		}
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		if (this.roles == null) {
	        this.roles = roles;
	    } else {
	        this.roles.clear();
	        this.roles.addAll(roles);
	    }
	}
	
	public synchronized void addRole(Role role) {
		getRoles().add(role);
	}

	public static class AccountBuilder {

    	private String username;
    	private String firstName;
    	private String lastName;
    	private String email;
    	private String phoneNumber;
    	private String password;
    	private Set<Role> roles;
    	private Boolean isVerified;
    	private Integer dateofbirthmonth;
        private Integer dateofbirthyear;
        private Integer dateofbirthday;
    	
        public AccountBuilder() {
        	super();
        }

        public AccountBuilder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public AccountBuilder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }
        
        public AccountBuilder email(String email) {
            this.email = email;
            return this;
        }
        
        public AccountBuilder password(String password) {
            this.password = password;
            return this;
        }
        
        public AccountBuilder phoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }
        
        public AccountBuilder roles(Set<Role> roles) {
            this.roles = roles;
            return this;
        }
        
        public AccountBuilder isVerified(Boolean isVerified) {
            this.isVerified = isVerified;
            return this;
        }
        
        public AccountBuilder username(String username) {
            this.username = username;
            return this;
        }
 
        public AccountBuilder dateofbirthmonth(Integer dateofbirthmonth) {
            this.dateofbirthmonth = dateofbirthmonth;
            return this;
        }        
        
        public AccountBuilder dateofbirthyear(Integer dateofbirthyear) {
            this.dateofbirthyear = dateofbirthyear;
            return this;
        }
        
        public AccountBuilder dateofbirthday(Integer dateofbirthday) {
            this.dateofbirthday = dateofbirthday;
            return this;
        }
        
        public Account build() {
            return new Account(this);
        }
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean isVerified() {
		return isVerified;
	}

	public void setVerified(Boolean isVerified) {
		this.isVerified = isVerified;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Integer getDateofbirthmonth() {
		return dateofbirthmonth;
	}

	public void setDateofbirthmonth(Integer dateofbirthmonth) {
		this.dateofbirthmonth = dateofbirthmonth;
	}

	public Integer getDateofbirthyear() {
		return dateofbirthyear;
	}

	public void setDateofbirthyear(Integer dateofbirthyear) {
		this.dateofbirthyear = dateofbirthyear;
	}

	public Integer getDateofbirthday() {
		return dateofbirthday;
	}

	public void setDateofbirthday(Integer dateofbirthday) {
		this.dateofbirthday = dateofbirthday;
	}

	public Boolean getBlacklisted() {
		return blacklisted;
	}

	public void setBlacklisted(Boolean blacklisted) {
		this.blacklisted = blacklisted;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public Boolean getIsVerified() {
		return isVerified;
	}

	public void setIsVerified(Boolean isVerified) {
		this.isVerified = isVerified;
	}

	public Boolean getHasAvatar() {
		if (hasAvatar == null) {
			return false;
		}
		return hasAvatar;
	}

	public void setHasAvatar(Boolean hasAvatar) {
		this.hasAvatar = hasAvatar;
	}

	public Set<Event> getEvents() {
		return events;
	}

	public synchronized void setEvents(Set<Event> events) {
		this.events = events;
	}
	
	public synchronized void addEvent(Event event) {
		events.add(event);
	}

	@Override
    public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).
				   appendSuper(super.toString()).
			       append("id", id).
			       append("username", username).
			       append("firstName", firstName).
			       append("lastName", lastName).
			       append("email", email).
			       append("roles", roles).
			       append("isverified", isVerified).
			       append("phoneNumber", phoneNumber).
			       append("dateofbirthmonth", dateofbirthmonth).
			       append("dateofbirthyear", dateofbirthyear).
			       append("dateofbirthday", dateofbirthday).
			       append("blacklisted", blacklisted).
			       append("locale", locale).
			       append("hasAvatar", hasAvatar).
			       append("password", "PROTECTED").
			       toString();
    }

	public Event findEventOrThrow(Long eventId) {
		Event event = findEvent(eventId);
		
		if (event == null) {
			throw new EventNotFoundException(eventId, ErrorCodes.EVENT_NOT_FOUND);
		}
		
		return event;
	}

	private Event findEvent(Long eventId) {
		if (events == null || events.size() == 0) {
			return null;
		}
		
		Event event = null;
		for (Event e : events) {
			if (e.getId().equals(eventId)) {
				event = e;
				break;
			}
		}
		return event;
	}
}