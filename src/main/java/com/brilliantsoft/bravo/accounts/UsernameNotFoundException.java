package com.brilliantsoft.bravo.accounts;

import com.brilliantsoft.bravo.exceptions.BaseException;

public class UsernameNotFoundException extends BaseException {
	private static final long serialVersionUID = 1L;

	public UsernameNotFoundException(String username, String code) {
		super("The user you requested with username " + username + " was not found in the database", code);
	}
}
