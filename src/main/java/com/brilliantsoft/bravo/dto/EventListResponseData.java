package com.brilliantsoft.bravo.dto;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class EventListResponseData extends ResponseData {

	private static final long serialVersionUID = 1L;

	private List<EventInfo> events;
	
	public EventListResponseData() {
		super();
	}

	public EventListResponseData(List<EventInfo> events) {
		super();
		this.events = events;
	}

	public List<EventInfo> getEvents() {
		return events;
	}

	public void setEvents(List<EventInfo> events) {
		this.events = events;
	}
	
	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
				.appendSuper(super.toString())
				.append("events", events)
				.toString();
	}
}