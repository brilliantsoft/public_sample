package com.brilliantsoft.bravo.dto;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class APIError implements Serializable {

	private static final long serialVersionUID = 1L;

	private String code;
	
	private String description;
	
	public APIError() {
		super();
	}

	public APIError(String code, String description) {
		super();
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
				.append("code", code)
				.append("description", description)
				.toString();
	}
}