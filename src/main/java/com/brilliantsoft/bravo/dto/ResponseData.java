package com.brilliantsoft.bravo.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class ResponseData implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<APIError> errors = new ArrayList<>(0);

	public List<APIError> getErrors() {
		return errors;
	}

	public void setErrors(List<APIError> errors) {
		this.errors = errors;
	}
	
	public void addError(String code, String description) {
		this.errors.add(new APIError(code, description));
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
				.append("errors", errors)
				.toString();
	}
}