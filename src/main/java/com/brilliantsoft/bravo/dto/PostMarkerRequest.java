package com.brilliantsoft.bravo.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class PostMarkerRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull
    private Double latitude;

	@NotNull
    private Double longitude;

	@NotNull
    private String indication;

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getIndication() {
		return indication;
	}

	public void setIndication(String indication) {
		this.indication = indication;
	}
    
	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
				.append("latitude", latitude)
				.append("longitude", longitude)
				.append("indication", indication)
				.toString();
	}
}