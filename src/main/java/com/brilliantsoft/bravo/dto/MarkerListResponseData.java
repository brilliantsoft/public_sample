package com.brilliantsoft.bravo.dto;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class MarkerListResponseData extends ResponseData {

	private static final long serialVersionUID = 1L;

	private List<MarkerInfo> markers;
	
	public MarkerListResponseData() {
		super();
	}

	public MarkerListResponseData(List<MarkerInfo> markers) {
		super();
		this.markers = markers;
	}
	
	public List<MarkerInfo> getMarkers() {
		return markers;
	}

	public void setMarkers(List<MarkerInfo> markers) {
		this.markers = markers;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
				.appendSuper(super.toString())
				.append("markers", markers)
				.toString();
	}
}