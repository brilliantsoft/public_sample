package com.brilliantsoft.bravo.dto;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class AuthenticationResponseData extends ResponseData {

	private static final long serialVersionUID = 1L;
	
	private String accessToken;

	public AuthenticationResponseData() {
		super();
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
				.append("accessToken", "PROTECTED")
				.toString();
	}
}