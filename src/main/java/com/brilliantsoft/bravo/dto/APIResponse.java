package com.brilliantsoft.bravo.dto;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class APIResponse <T extends ResponseData> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Boolean success;

	private T data;
	
	private String code;
	
	private String description;
	
	public APIResponse() {
		super();
	}
	
	public APIResponse(Boolean success, String code, String description, T data) {
		super();
		this.success = success;
		this.data = data;
		this.code = code;
		this.description = description;
	}

	public T getData() {
		return data;
	}
	
	public void setData(T data) {
		this.data = data;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
				.append("success", success)
				.append("data", data)
				.append("code", code)
				.append("description", description)
				.toString();
	}
}