package com.brilliantsoft.bravo.dto;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class MarkerInfo {

    private Long id;
	
    private Double latitude;

    private Double longitude;

    private String indication;
    
	public MarkerInfo() {
		super();
	}

	public MarkerInfo(Long id, Double latitude, Double longitude, String indication) {
		super();
		this.id = id;
		this.latitude = latitude;
		this.longitude = longitude;
		this.indication = indication;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getIndication() {
		return indication;
	}

	public void setIndication(String indication) {
		this.indication = indication;
	}
    
	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
				.append("id", id)
				.append("latitude", latitude)
				.append("longitude", longitude)
				.append("indication", indication)
				.toString();
	}
}