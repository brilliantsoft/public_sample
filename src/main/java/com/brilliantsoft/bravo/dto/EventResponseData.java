package com.brilliantsoft.bravo.dto;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class EventResponseData extends ResponseData {

	private static final long serialVersionUID = 1L;
	
    private EventInfo event;
    
	public EventResponseData() {
		super();
	}
	
	public EventResponseData(EventInfo event) {
		this.event = event;
	}
	
	public EventInfo getEvent() {
		return event;
	}

	public void setEvent(EventInfo event) {
		this.event = event;
	}

	@Override
    public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).
				   appendSuper(super.toString()).
			       append("event", event).
			       toString();
    }
}