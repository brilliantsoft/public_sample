package com.brilliantsoft.bravo.config;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import com.brilliantsoft.bravo.accounts.Account;
import com.brilliantsoft.bravo.accounts.AccountRepository;
import com.brilliantsoft.bravo.accounts.Role;
import com.brilliantsoft.bravo.accounts.RoleRepository;
import com.brilliantsoft.bravo.accounts.Account.AccountBuilder;
import com.brilliantsoft.bravo.events.Event;
import com.brilliantsoft.bravo.events.EventRepository;

@Configuration
public class ApplicationDevConfig {

	@Bean
	@Transactional
	public CommandLineRunner demo(AccountRepository repository,
			RoleRepository roleRepository, EventRepository eventRepository,
			PasswordEncoder encoder) {
		return (args) -> {
			
			Role role = roleRepository.findByRolename("USER");
			if (role == null) {
				role = roleRepository.save(new Role("USER"));
			}
			
			Account account = repository.findByUsername("lfonseca@gmail.com");
			if (account == null) {
				account = new AccountBuilder()
						.username("lfonseca@gmail.com")
						.password(encoder.encode("password"))
						.build();
				account.addRole(role);
				
				repository.save(account);
			}
			
			Event event = eventRepository.findByName("Test");
			if (event == null) {
				eventRepository.save(new Event("Test", account));
			}
			
			Event event2 = eventRepository.findByName("Another");
			if (event2 == null) {
				eventRepository.save(new Event("Another", account));
			}
		};
	}
}