package com.brilliantsoft.bravo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@PropertySource("classpath:application.properties")
public class ApplicationProperties {
	
	@Value("${bravo.paths.event-logo}")
	private String eventLogoFolderPath;

	@Value("${bravo.paths.event-logo.max-size}")
	private Integer eventLogoMaxSize;

	@Value("${bravo.paths.event-logo.default-path}")
	private String avatarDefaultPath;

	@Value("${bravo.paths.tracks.path}")
	private String trackFolder;
	
	public String getEventLogoFolderPath() {
		return eventLogoFolderPath;
	}

	public Integer getEventLogoMaxSize() {
		return eventLogoMaxSize;
	}

	public String getAvatarDefaultPath() {
		return avatarDefaultPath;
	}

	public String getTrackFolder() {
		return trackFolder;
	}
	
	@Bean
    static PropertySourcesPlaceholderConfigurer propertyPlaceHolderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}