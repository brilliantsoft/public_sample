package com.brilliantsoft.bravo.config.db;

import javax.sql.DataSource;

public interface DataConfig {
	DataSource dataSource();
}
