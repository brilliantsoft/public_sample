package com.brilliantsoft.bravo.config.db;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("rdbms")
public class RdbmsDataConfig implements DataConfig {
	
	@Override
	@Bean
	@ConfigurationProperties(prefix = "bravo.datasource")
	public DataSource dataSource() {
		return DataSourceBuilder.create().build();
	}

}