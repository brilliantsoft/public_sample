package com.brilliantsoft.bravo.config.security;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.CompositeTokenGranter;
import org.springframework.security.oauth2.provider.TokenGranter;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices;
import org.springframework.security.oauth2.provider.code.JdbcAuthorizationCodeServices;
import org.springframework.security.oauth2.provider.error.DefaultWebResponseExceptionTranslator;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

@Configuration
@EnableAuthorizationServer
public class OAuth2Config extends AuthorizationServerConfigurerAdapter {
	
	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private DataSource dataSource;

	@Autowired
	private PasswordEncoder passwordEncoder;
	
/*
	@Bean
	public JwtAccessTokenConverter jwtAccessTokenConverter() {
		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();

		KeyPair keyPair = new KeyStoreKeyFactory(new ClassPathResource(
				"brilliantsoft.jks"), "brilliantsoft2017".toCharArray()).getKeyPair(
				"brilliantsoft", "brilliantsoft2017".toCharArray());

		converter.setKeyPair(keyPair);
		return converter;
	}
*/
	@Bean
	public JdbcTokenStore tokenStore() {
		return new JdbcTokenStore(dataSource);
	}
	
	@Bean
	protected AuthorizationCodeServices authorizationCodeServices() {
		return new JdbcAuthorizationCodeServices(dataSource);
	}

	@Override
	public void configure(AuthorizationServerSecurityConfigurer security)
			throws Exception {
		security.tokenKeyAccess("permitAll()")
				.checkTokenAccess("isAuthenticated()")
				.passwordEncoder(passwordEncoder);
	}
	
	@Bean
	public WebResponseExceptionTranslator webResponseExceptionTranslator() {
	     return new DefaultWebResponseExceptionTranslator() {

	         @Override
	         public ResponseEntity<OAuth2Exception> translate(Exception e) throws Exception {
	        	 
	             ResponseEntity<OAuth2Exception> responseEntity = super.translate(e);
	             
	             OAuth2Exception body = responseEntity.getBody();
	             HttpHeaders headers = new HttpHeaders();
	             headers.setAll(responseEntity.getHeaders().toSingleValueMap());
	             // do something with header or response
	        	 
	             body.addAdditionalInformation("status", String.valueOf(body.getHttpErrorCode()));
	             body.addAdditionalInformation("code", body.getOAuth2ErrorCode());
	             body.addAdditionalInformation("message", String.valueOf(body.getMessage()));
	             body.addAdditionalInformation("developerMessage", body.getMessage());
	             
	        	 
	             return new ResponseEntity<>(body, headers, responseEntity.getStatusCode());
	         }
	     };
	 }

	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints)
			throws Exception {

		endpoints.authorizationCodeServices(authorizationCodeServices())
				.authenticationManager(authenticationManager)
				//.accessTokenConverter(jwtAccessTokenConverter())
				.tokenStore(tokenStore()).approvalStoreDisabled()
				.tokenGranter(tokenGranter(endpoints))
                .exceptionTranslator(webResponseExceptionTranslator());
	}

	@Override
	public void configure(ClientDetailsServiceConfigurer clients)
			throws Exception {
		clients.jdbc(dataSource).passwordEncoder(passwordEncoder);
	}

	private TokenGranter tokenGranter(
			final AuthorizationServerEndpointsConfigurer endpoints) {
		List<TokenGranter> granters = new ArrayList<TokenGranter>(
				Arrays.asList(endpoints.getTokenGranter()));
		return new CompositeTokenGranter(granters);
	}
}