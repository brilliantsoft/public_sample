package com.brilliantsoft.bravo.config.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

	private static final String RESOURCE_ID = "alpha/apis";
	
	@Override
	public void configure(ResourceServerSecurityConfigurer resources)
			throws Exception {
		// @formatter:off
		resources.resourceId(RESOURCE_ID);
		// @formatter:on
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		
		http
		.authorizeRequests()
        	.antMatchers("/",
        			     "/index",
        			     "/api/token",
        			     "/api/events/**/logo",
        			     "/api/events/**/track",
        			     "/resources/**", 
        			     "/partials/**")
				.permitAll()
		.anyRequest()
			.authenticated();
	}
	
	@Bean
    HttpSessionSecurityContextRepository contextRepository() {
        return new HttpSessionSecurityContextRepository();
    }

}