package com.brilliantsoft.bravo.events;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.brilliantsoft.bravo.accounts.Account;
import com.brilliantsoft.bravo.accounts.AccountRepository;
import com.brilliantsoft.bravo.core.FileSystemService;
import com.brilliantsoft.bravo.exceptions.EventNotFoundException;
import com.brilliantsoft.bravo.exceptions.ExceptionConstants.ErrorCodes;

@Service
public class EventService {

	private EventRepository eventRepository;
	private AccountRepository accountRepository;
	private FileSystemService fileSystemService;
	private MarkerRepository markerRepository;
	
	@Autowired
	public EventService(EventRepository eventRepository, AccountRepository accountRepository,
			MarkerRepository markerRepository, FileSystemService fileSystemService) {
		super();
		this.eventRepository = eventRepository;
		this.accountRepository = accountRepository;
		this.fileSystemService = fileSystemService;
		this.markerRepository = markerRepository;
	}

	public List<Event> findEventsByUser(String username) {
		assert username != null;
		
		Account account = accountRepository.findByUsername(username);
		return eventRepository.findByAccount(account);
	}

	public Event newEvent(Event event, String username) {
		assert event != null;
		assert username != null;
		
		Account account = accountRepository.findByUsername(username);
		event.setAccount(account);
		
		return eventRepository.save(event);
	}

	public void saveEventLogo(MultipartFile file, Long eventId, String username) throws IOException {
		assert username != null;
		assert file != null;
		assert eventId != null;
				
		Account account = accountRepository.findByUsername(username);
		Event event = account.findEventOrThrow(eventId);
				
		fileSystemService.saveEventLogo(file, eventId);
		
		event.setHasImage(true);
		eventRepository.save(event);
	}

	public byte[] getEventLogo(Long eventId, Optional<Integer> scale) throws IOException {
		assert eventId != null;
		
		byte[] avatar = null;
				
		Event event = this.findEventOrThrow(eventId);
				
		if (event.getHasImage() == null || !event.getHasImage()) {
			avatar = fileSystemService.getDefaultLogo(scale);
		} else {
			avatar = fileSystemService.loadEventLogo(eventId, scale);
		}
		
		return avatar;
	}

	public void uploadEventTrack(MultipartFile file, Long eventId, String username) throws IOException {
		assert username != null;
		assert file != null;
		assert eventId != null;
				
		Account account = accountRepository.findByUsername(username);
		Event event = account.findEventOrThrow(eventId);
				
		String fileName = UUID.randomUUID().toString();
		fileSystemService.saveTrackFile(file, fileName);
		
		event.setTrackFileName(fileName);
		eventRepository.save(event);
	}

	public byte[] getEventTrack(String username, Long eventId) throws IOException {
		assert username != null;
		assert eventId != null;
		
		byte[] trackFile = null;
				
		Account account = accountRepository.findByUsername(username);
		Event event = account.findEventOrThrow(eventId);
				
		if (event.getTrackFileName() != null) {
			trackFile = fileSystemService.getTrackFile(event.getTrackFileName());
		}
		
		return trackFile;
	}

	public Event findEvent(Long eventId, String username) {
		assert eventId != null;
		assert username != null;
		
		Account account = accountRepository.findByUsername(username);
		return account.findEventOrThrow(eventId);
	}

	public Marker newMarker(Marker marker, Long eventId, String username) {
		assert marker != null;
		assert username != null;
		
		Account account = accountRepository.findByUsername(username);
		Event event = account.findEventOrThrow(eventId);
		
		marker.setEvent(event);
		
		return markerRepository.save(marker);
	}

	public List<Marker> findMarkersByEvent(Long eventId, String username) {
		assert eventId != null;
		assert username != null;
		
		Account account = accountRepository.findByUsername(username);
		Event event = account.findEventOrThrow(eventId);
		
		return markerRepository.findByEvent(event);
	}

	private Event findEventOrThrow(Long eventId) {
		assert eventId != null;
		
		Event event = eventRepository.findOne(eventId);
		if (event == null) {
			throw new EventNotFoundException(eventId, ErrorCodes.EVENT_NOT_FOUND);
		}
		return event;
	}
}