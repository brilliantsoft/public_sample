package com.brilliantsoft.bravo.events;

public enum EventStatus {
	created, live, archived
}
