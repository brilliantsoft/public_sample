package com.brilliantsoft.bravo.events;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.brilliantsoft.bravo.accounts.Account;

public interface EventRepository extends JpaRepository<Event, Long> {

	List<Event> findByAccount(Account account);

	Event findByName(String name);

}