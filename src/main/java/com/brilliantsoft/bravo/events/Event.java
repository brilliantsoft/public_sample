package com.brilliantsoft.bravo.events;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.brilliantsoft.bravo.accounts.Account;
import com.brilliantsoft.bravo.data.BaseEntity;

@Entity
@Table(name = "event")
public class Event extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
	
	@Column(name="name")
    private String name;
	
	@Column(name="hasimage")
    private Boolean hasImage;

	@Column(name="trackfilename")
    private String trackFileName;

	@Column(name="status")
	@Enumerated(value = EnumType.STRING)
    private EventStatus status;

    @Column(name = "eventdate")
    private LocalDateTime eventDate;

	@Column(name="url")
    private String url;

	@Column(name="phone")
    private String phone;
	
	@Column(name="email")
    private String email;
	
	@ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "accountid", nullable = false, updatable = false)
	private Account account;
	
	@OneToMany(mappedBy = "event", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.TRUE)
	private Set<Marker> markers = new HashSet<Marker>();
	
    public Event() {
    	super();
    }

	public Event(String name, Account account) {
		super();
		this.name = name;
		this.account = account;
	}

	public Event(String name) {
		super();
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getHasImage() {
		return hasImage;
	}

	public void setHasImage(Boolean hasImage) {
		this.hasImage = hasImage;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public String getTrackFileName() {
		return trackFileName;
	}

	public void setTrackFileName(String trackFileName) {
		this.trackFileName = trackFileName;
	}

	public Set<Marker> getMarkers() {
		return markers;
	}

	public void setMarkers(Set<Marker> markers) {
		this.markers = markers;
	}

	public EventStatus getStatus() {
		return status;
	}

	public void setStatus(EventStatus status) {
		this.status = status;
	}

	public LocalDateTime getEventDate() {
		return eventDate;
	}

	public void setEventDate(LocalDateTime eventDate) {
		this.eventDate = eventDate;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
    public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).
				   appendSuper(super.toString()).
			       append("id", id).
			       append("name", name).
			       append("hasImage", hasImage).
			       append("trackFileName", trackFileName).
			       append("phone", phone).
			       append("email", email).
			       append("url", url).
			       append("eventDate", eventDate).
			       toString();
    }
}