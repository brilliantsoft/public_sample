package com.brilliantsoft.bravo.events;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MarkerRepository extends JpaRepository<Marker, Long> {

	List<Marker> findByEvent(Event event);

}