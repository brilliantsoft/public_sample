package com.brilliantsoft.bravo.events;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.brilliantsoft.bravo.data.BaseEntity;

@Entity
@Table(name = "marker")
public class Marker extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
	
	@Column(name="latitude")
    private Double latitude;

	@Column(name="longitude")
    private Double longitude;

	@Column(name="indication")
    private String indication;
	
	@ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "eventid", nullable = false, updatable = false)
	private Event event;
	
    public Marker() {
    	super();
    }

	public Marker(Double latitude, Double longitude, String indication) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
		this.indication = indication;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getIndication() {
		return indication;
	}

	public void setIndication(String indication) {
		this.indication = indication;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	@Override
    public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).
				   appendSuper(super.toString()).
			       append("id", id).
			       append("latitude", latitude).
			       append("longitude", longitude).
			       append("indication", indication).
			       toString();
    }
}