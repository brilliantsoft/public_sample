package com.brilliantsoft.bravo.async;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.brilliantsoft.bravo.accounts.Account;
import com.brilliantsoft.bravo.accounts.AccountService;
import com.brilliantsoft.bravo.dto.Credentials;
import com.brilliantsoft.bravo.dto.UserToken;

import rx.Observable;

@Service
public class AccountServiceAsync {
	private AccountService accountService;
	
	@Autowired
	public AccountServiceAsync(AccountService accountService){
		this.accountService = accountService;
	}

	public Observable<Account> findAccount(String name) {
		return Observable.<Account>create(s -> {
			try {
				s.onNext(accountService.findAccount(name));
			    s.onCompleted();
			} catch (Exception e) {
				s.onError(e);
			}
		});
	}

	public Observable<UserToken> authenticate(Credentials credentials) {
		return Observable.<UserToken>create(s -> {
			try {
				s.onNext(accountService.authenticate(credentials));
			    s.onCompleted();
			} catch (Exception e) {
				s.onError(e);
			}
		});
	}
}
