package com.brilliantsoft.bravo.async;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.brilliantsoft.bravo.events.Event;
import com.brilliantsoft.bravo.events.EventService;
import com.brilliantsoft.bravo.events.Marker;

import rx.Observable;

@Service
public class EventServiceAsync {
	
	private EventService eventService;
	
	@Autowired
	public EventServiceAsync(EventService eventService){
		this.eventService = eventService;
	}

	public Observable<List<Event>> findEventsByUser(String name) {
		return Observable.<List<Event>>create(s -> {
			try {
				s.onNext(eventService.findEventsByUser(name));
			    s.onCompleted();
			} catch (Exception e) {
				s.onError(e);
			}
		});
	}

	public Observable<Event> newEvent(Event event, String username) {
		return Observable.<Event>create(s -> {
			try {
				s.onNext(eventService.newEvent(event, username));
			    s.onCompleted();
			} catch (Exception e) {
				s.onError(e);
			}
		});
	}

	public Observable<Void> saveEventLogo(MultipartFile file, Long eventId, String username) {
		return Observable.<Void>create(s -> {
			try {
				eventService.saveEventLogo(file, eventId, username);
			    s.onCompleted();
			} catch (Exception e) {
				s.onError(e);
			}
		});
	}

	public Observable<byte[]> getEventLogo(Long eventId, Optional<Integer> scale) {
		return Observable.<byte[]>create(s -> {
			try {
				s.onNext(eventService.getEventLogo(eventId, scale));
			    s.onCompleted();
			} catch (Exception e) {
				s.onError(e);
			}
		});
	}

	public Observable<Void> uploadEventTrack(MultipartFile file, Long eventId, String username) {
		return Observable.<Void>create(s -> {
			try {
				eventService.uploadEventTrack(file, eventId, username);
			    s.onCompleted();
			} catch (Exception e) {
				s.onError(e);
			}
		});
	}

	public Observable<byte[]> getEventTrack(String username, Long eventId) {
		return Observable.<byte[]>create(s -> {
			try {
				s.onNext(eventService.getEventTrack(username, eventId));
			    s.onCompleted();
			} catch (Exception e) {
				s.onError(e);
			}
		});
	}

	public Observable<Event> findEvent(Long eventId, String username) {
		return Observable.<Event>create(s -> {
			try {
				s.onNext(eventService.findEvent(eventId, username));
			    s.onCompleted();
			} catch (Exception e) {
				s.onError(e);
			}
		});
	}

	public Observable<Marker> newMarker(Marker marker, Long eventId, String username) {
		return Observable.<Marker>create(s -> {
			try {
				s.onNext(eventService.newMarker(marker, eventId, username));
			    s.onCompleted();
			} catch (Exception e) {
				s.onError(e);
			}
		});
	}

	public Observable<List<Marker>> findMarkersByEvent(Long eventId, String username) {
		return Observable.<List<Marker>>create(s -> {
			try {
				s.onNext(eventService.findMarkersByEvent(eventId, username));
			    s.onCompleted();
			} catch (Exception e) {
				s.onError(e);
			}
		});
	}
}