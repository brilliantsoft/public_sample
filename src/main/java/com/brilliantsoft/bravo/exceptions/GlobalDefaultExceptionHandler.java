package com.brilliantsoft.bravo.exceptions;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.brilliantsoft.bravo.dto.APIResponse;
import com.brilliantsoft.bravo.dto.ResponseData;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;

@ControllerAdvice
public class GlobalDefaultExceptionHandler {
    
	private MessageSource messageSource;
	 
    @Autowired
    public GlobalDefaultExceptionHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }
    // CUSTOM EXCEPTIONS
    
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(InvalidCredentialsException.class)
    @ResponseBody APIResponse<?> handle(HttpServletRequest req, InvalidCredentialsException ex) {
        return new APIResponse<>(false, "0100", "Invalid credentials", null);
    }
    
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(EventNotFoundException.class)
    @ResponseBody APIResponse<?> handle(HttpServletRequest req, EventNotFoundException ex) {
        return new APIResponse<>(false, "0101", "Event not found", null);
    }
    // END CUSTOM EXCEPTIONS
    
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseBody APIResponse<?> handle(HttpServletRequest req, HttpMessageNotReadableException ex) {
        return new APIResponse<>(false, "0001", "Required request body is missing", null);
    }
    
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseBody APIResponse<?> handle(HttpServletRequest req, IllegalArgumentException ex) {
        return new APIResponse<>(false, "0002", "Required information is missing", null);
    }
    
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public APIResponse<?> handle(MethodArgumentNotValidException ex) {
        BindingResult result = ex.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();
 
        return new APIResponse<>(false, "0003", "Bad request", processFieldErrors(fieldErrors));
    }
    
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    @ResponseBody APIResponse<?> handle(HttpServletRequest req, Exception ex) {
        return new APIResponse<>(false, "0004", "Server error. Please try again", null);
    }
    
    private ResponseData processFieldErrors(List<FieldError> fieldErrors) {
    	ResponseData dto = new ResponseData();
 
        for (FieldError fieldError: fieldErrors) {
            String localizedErrorMessage = resolveLocalizedErrorMessage(fieldError);
            dto.addError(fieldError.getField(), localizedErrorMessage);
        }
        return dto;
    }
 
    private String resolveLocalizedErrorMessage(FieldError fieldError) {
        Locale currentLocale =  LocaleContextHolder.getLocale();
        String localizedErrorMessage = messageSource.getMessage(fieldError, currentLocale);
 
        return localizedErrorMessage;
    }
}