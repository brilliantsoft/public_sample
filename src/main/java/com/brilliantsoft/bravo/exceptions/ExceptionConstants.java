package com.brilliantsoft.bravo.exceptions;

public class ExceptionConstants {
	
	public static final int GENERIC_APP_ERROR_CODE = 5001;
	public static final String HELP_URL = "http://www.airbanq.com/apis/uaa";
	
	
	public static final class ErrorCodes {

		public static final String MISSING_REQUEST_BODY = "MISSING_REQUEST_BODY";
		public static final String MISSING_REQUEST_DATA = "MISSING_REQUEST_DATA";
		public static final String REQUEST_BODY_VALIDATION_FAILED = "REQUEST_BODY_VALIDATION_FAILED";
		public static final String EVENT_NOT_FOUND = "EVENT_NOT_FOUND";
		public static final String INVALID_CREDENTIALS = "INVALID_CREDENTIALS";
		
	}

}