package com.brilliantsoft.bravo.exceptions;

public class InvalidCredentialsException extends BaseException {

	private static final long serialVersionUID = 1L;

	public InvalidCredentialsException(String code) {
		super("Invalid credentials", code);
	}
}