package com.brilliantsoft.bravo.exceptions;

public class EventNotFoundException extends BaseException {

	private static final long serialVersionUID = 1L;

	public EventNotFoundException(Long eventId, String code) {
		super("The event with id " + eventId + " was not found in the database", code);
	}

}