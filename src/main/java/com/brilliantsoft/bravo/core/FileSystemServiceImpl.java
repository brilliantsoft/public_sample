package com.brilliantsoft.bravo.core;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.imgscalr.Scalr;
import org.springframework.web.multipart.MultipartFile;

import com.brilliantsoft.bravo.config.ApplicationProperties;

public class FileSystemServiceImpl implements FileSystemService {
		
	private ApplicationProperties applicationProperties;
	
	public FileSystemServiceImpl(ApplicationProperties applicationProperties){
		this.applicationProperties = applicationProperties;
	}

	@Override
	public String saveEventLogo(MultipartFile file, Long identifier) throws IOException {
		assert file != null;
		
		InputStream in = new ByteArrayInputStream(file.getBytes()); 
		
		BufferedImage escaladeImage = getEscaladeImage(in, applicationProperties.getEventLogoMaxSize());
		
		String destination = applicationProperties.getEventLogoFolderPath() + identifier + ".jpeg";
		
		ImageIO.write(escaladeImage, "jpeg", new File(destination));

		return destination;
	}

	@Override
	public byte[] getDefaultLogo(Optional<Integer> scale) throws IOException {
		return loadAvatarInternal(applicationProperties.getAvatarDefaultPath(), scale);
	}

	@Override
	public byte[] loadEventLogo(Long identifier, Optional<Integer> scale) throws IOException {
		return loadAvatarInternal(applicationProperties.getEventLogoFolderPath() + identifier + ".jpeg", scale);
	}

	@Override
	public void saveTrackFile(MultipartFile multipartFile, String name) throws IOException {
		String fileNameToCreate = applicationProperties.getTrackFolder() + name + ".kml";
		File file = new File(fileNameToCreate);
		FileUtils.writeByteArrayToFile(file, multipartFile.getBytes());
	}

	@Override
	public byte[] getTrackFile(String name) throws IOException {
		String fileNameToRead = applicationProperties.getTrackFolder() + name + ".kml";
		File file = new File(fileNameToRead);
		return FileUtils.readFileToByteArray(file);
	}
		    
	private byte[] loadAvatarInternal(String avatarPath, Optional<Integer> scale) throws IOException {
		byte[] imageInByte = null;
		
		try(ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			Integer size = scale.isPresent()? scale.get() : applicationProperties.getEventLogoMaxSize();
			BufferedImage in = ImageIO.read(new File(avatarPath));
			BufferedImage escaladeImage = scale(in, size);
					
			ImageIO.write(escaladeImage, "jpeg", baos);
			baos.flush();
			imageInByte = baos.toByteArray();
		}
		
		return imageInByte;
	}
	
	private BufferedImage getEscaladeImage(InputStream in, Integer scale) throws IOException {
        return scale(ImageIO.read(in), scale);
	}
	
	private BufferedImage scale(BufferedImage in, Integer scale) {
		return Scalr.resize(in, scale);
	}
}