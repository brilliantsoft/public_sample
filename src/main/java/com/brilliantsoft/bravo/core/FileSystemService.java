package com.brilliantsoft.bravo.core;

import java.io.IOException;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

public interface FileSystemService {

	String saveEventLogo(MultipartFile file, Long identifier) throws IOException;

	byte[] getDefaultLogo(Optional<Integer> scale) throws IOException;

	byte[] loadEventLogo(Long identifier, Optional<Integer> scale) throws IOException;

	void saveTrackFile(MultipartFile multipartFile, String name) throws IOException;

	byte[] getTrackFile(String name) throws IOException;

}