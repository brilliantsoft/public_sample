package com.brilliantsoft.bravo.common;

import com.brilliantsoft.bravo.dto.APIResponse;
import com.brilliantsoft.bravo.dto.ResponseData;

public class ResponseUtils {

	public static <T extends ResponseData> APIResponse<T> createSuccessResponse(T responseData) {
		APIResponse<T> apiResponse = new APIResponse<>();
		apiResponse.setCode("0000");
		apiResponse.setData(responseData);
		apiResponse.setDescription("Success");
		apiResponse.setSuccess(true);
		
		return apiResponse;
	}
	
}