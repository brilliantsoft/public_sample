package com.brilliantsoft.bravo.common;

import java.util.ArrayList;
import java.util.List;

import com.brilliantsoft.bravo.accounts.Account;
import com.brilliantsoft.bravo.dto.AuthenticationResponseData;
import com.brilliantsoft.bravo.dto.EventInfo;
import com.brilliantsoft.bravo.dto.MarkerInfo;
import com.brilliantsoft.bravo.dto.PostEventRequest;
import com.brilliantsoft.bravo.dto.PostMarkerRequest;
import com.brilliantsoft.bravo.dto.UserInfoResponseData;
import com.brilliantsoft.bravo.dto.UserToken;
import com.brilliantsoft.bravo.events.Event;
import com.brilliantsoft.bravo.events.Marker;

public class MapUtils {

	public static List<EventInfo> mapToEventListDto(List<Event> infoList) {
		List<EventInfo> events = new ArrayList<>();
		for (Event event : infoList) {
			events.add(mapToDto(event));
		}
		return events;
	}

	public static EventInfo mapToDto(Event event) {
		EventInfo info = new EventInfo();
		info.setId(event.getId());
		info.setName(event.getName());
		info.setStatus(event.getStatus());
		info.setTrackID(event.getTrackFileName());
		return info;
	}

	public static Event mapToDomain(PostEventRequest request) {
		return new Event(request.getName());
	}

	public static Marker mapToDomain(PostMarkerRequest request) {
		return new Marker(request.getLatitude(), request.getLongitude(), request.getIndication());
	}

	public static List<MarkerInfo> mapToMarkerListDto(List<Marker> infoList) {
		List<MarkerInfo> markers = new ArrayList<>();
		for (Marker marker : infoList) {
			markers.add(mapToDto(marker));
		}
		return markers;
	}

	public static MarkerInfo mapToDto(Marker marker) {
		return new MarkerInfo(marker.getId(), marker.getLatitude(), marker.getLongitude(), marker.getIndication());
	}

	public static UserInfoResponseData mapToDto(Account account) {
		UserInfoResponseData userInfo = new UserInfoResponseData();
		userInfo.setUsername(account.getUsername());
		userInfo.setEmail(account.getEmail());
		userInfo.setLastName(account.getLastName());
		return userInfo;
	}

	public static AuthenticationResponseData mapToDto(UserToken token) {
		AuthenticationResponseData data = new AuthenticationResponseData();
		data.setAccessToken(token.getAccessToken());
		return data;
	}
}