package com.brilliantsoft.bravo.api;

import static org.springframework.http.HttpStatus.OK;

import java.security.Principal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.async.DeferredResult;

import com.brilliantsoft.bravo.async.AccountServiceAsync;
import com.brilliantsoft.bravo.common.LoggableDeferredResult;
import com.brilliantsoft.bravo.common.MapUtils;
import com.brilliantsoft.bravo.common.ResponseUtils;
import com.brilliantsoft.bravo.dto.APIResponse;
import com.brilliantsoft.bravo.dto.AuthenticationResponseData;
import com.brilliantsoft.bravo.dto.Credentials;
import com.brilliantsoft.bravo.dto.ResponseData;
import com.brilliantsoft.bravo.dto.UserInfoResponseData;

@Controller
@RequestMapping("/api")
public class AccountController {
	
	private AccountServiceAsync accountService;
	
	@Autowired
	public AccountController(AccountServiceAsync accountService) {
		super();
		this.accountService = accountService;
	}
	
	@RequestMapping(value = "/authenticated", method = RequestMethod.GET)
	@ResponseStatus(OK)
	@ResponseBody
	public APIResponse<ResponseData> getEvents(Principal principal) {
		return ResponseUtils.<ResponseData>createSuccessResponse(new ResponseData());
	}
	
	@RequestMapping(value = "/account", method = RequestMethod.GET)
	@ResponseStatus(OK)
	@ResponseBody
	public DeferredResult<APIResponse<UserInfoResponseData>> getAccount(Principal principal) {
		
		DeferredResult<APIResponse<UserInfoResponseData>> result = new LoggableDeferredResult<>();
		
		accountService.findAccount(principal.getName()).subscribe(
				m -> result.setResult(ResponseUtils.<UserInfoResponseData> createSuccessResponse(MapUtils.mapToDto(m))),
				e -> result.setErrorResult(e));

		return result;
	}
	
	@RequestMapping(value = "/token", method = RequestMethod.POST)
	@ResponseStatus(OK)
	@ResponseBody
	public DeferredResult<APIResponse<AuthenticationResponseData>> getAccount(@Valid @RequestBody Credentials credentials) {
		
		DeferredResult<APIResponse<AuthenticationResponseData>> result = new LoggableDeferredResult<>();
		
		accountService.authenticate(credentials).subscribe(
				m -> result.setResult(
						ResponseUtils.<AuthenticationResponseData> createSuccessResponse(MapUtils.mapToDto(m))),
				e -> result.setErrorResult(e));

		return result;
	}
}