package com.brilliantsoft.bravo.api;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.CREATED;

import java.security.Principal;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.multipart.MultipartFile;

import com.brilliantsoft.bravo.async.EventServiceAsync;
import com.brilliantsoft.bravo.common.LoggableDeferredResult;
import com.brilliantsoft.bravo.common.MapUtils;
import com.brilliantsoft.bravo.common.ResponseUtils;
import com.brilliantsoft.bravo.dto.APIResponse;
import com.brilliantsoft.bravo.dto.EventListResponseData;
import com.brilliantsoft.bravo.dto.EventResponseData;
import com.brilliantsoft.bravo.dto.MarkerListResponseData;
import com.brilliantsoft.bravo.dto.NewEventResponseData;
import com.brilliantsoft.bravo.dto.PostEventRequest;
import com.brilliantsoft.bravo.dto.PostMarkerRequest;
import com.brilliantsoft.bravo.dto.ResponseData;

@Controller
@RequestMapping("/api")
public class EventController {
	
	private EventServiceAsync eventService;
	
	@Autowired
	public EventController(EventServiceAsync eventService) {
		super();
		this.eventService = eventService;
	}
	
	@RequestMapping(value = "/events", method = RequestMethod.GET)
	@ResponseStatus(OK)
	@ResponseBody
	public DeferredResult<APIResponse<EventListResponseData>> getEvents(Principal principal) {
		
		DeferredResult<APIResponse<EventListResponseData>> result = new LoggableDeferredResult<>();
		
		eventService.findEventsByUser(principal.getName())
				.subscribe(m -> {
						EventListResponseData response = new EventListResponseData(MapUtils.mapToEventListDto(m));
						result.setResult(ResponseUtils.<EventListResponseData>createSuccessResponse(response)); 
					},
						   e -> result.setErrorResult(e));
		
		return result;
	}
	
	@RequestMapping(value = "/events", method = RequestMethod.POST)
	@ResponseStatus(CREATED)
	@ResponseBody
	public DeferredResult<APIResponse<NewEventResponseData>> postEvent(@Valid @RequestBody PostEventRequest request, Principal principal) {
		
		DeferredResult<APIResponse<NewEventResponseData>> result = new LoggableDeferredResult<>();
		
		eventService.newEvent(MapUtils.mapToDomain(request), principal.getName())
				.subscribe(m -> { 
						NewEventResponseData responseData = new NewEventResponseData(MapUtils.mapToDto(m));
						result.setResult(ResponseUtils.<NewEventResponseData>createSuccessResponse(responseData));
					},
						   e -> result.setErrorResult(e));
		
		return result;
	}
	
	@RequestMapping(value = "/events/{eventId}", method = RequestMethod.GET)
	@ResponseStatus(OK)
	@ResponseBody
	public DeferredResult<APIResponse<EventResponseData>> getEvent(@PathVariable("eventId") Long eventId, Principal principal) {
		
		DeferredResult<APIResponse<EventResponseData>> result = new LoggableDeferredResult<>();
		
		eventService.findEvent(eventId, principal.getName())
				.subscribe(m -> {
						EventResponseData response = new EventResponseData(MapUtils.mapToDto(m));
						result.setResult(ResponseUtils.<EventResponseData>createSuccessResponse(response)); 
					},
						   e -> result.setErrorResult(e));
		
		return result;
	}
	
	@RequestMapping(value = "/events/{eventId}/logo", method = RequestMethod.POST)
	@ResponseStatus(OK)
	@ResponseBody
	public DeferredResult<APIResponse<ResponseData>> uploadEventLogo(@RequestParam("image") MultipartFile file, 
			@PathVariable("eventId") Long eventId, Principal principal) {

		DeferredResult<APIResponse<ResponseData>> result = new LoggableDeferredResult<>();
		
		eventService.saveEventLogo(file, eventId, principal.getName())
			.subscribe(m -> {}, 
				       e -> result.setErrorResult(e),
				       () -> result.setResult(ResponseUtils.<ResponseData>createSuccessResponse(new ResponseData())));

		return result;
	}

	@RequestMapping(value = "/events/{eventId}/logo", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
	@ResponseStatus(OK)
	@ResponseBody
	public DeferredResult<byte[]> getEventLogo(@RequestParam("scale") Optional<Integer> scale,
			@PathVariable("eventId") Long eventId) {

		DeferredResult<byte[]> result = new LoggableDeferredResult<>();
		
		eventService.getEventLogo(eventId, scale)
			.subscribe(m -> result.setResult(m), 
				       e -> result.setErrorResult(e));

		return result;
	}
	
	@RequestMapping(value = "/events/{eventId}/track", method = RequestMethod.POST)
	@ResponseStatus(OK)
	@ResponseBody
	public DeferredResult<APIResponse<ResponseData>> uploadEventTrack(@RequestParam("file") MultipartFile file, 
			@PathVariable("eventId") Long eventId, Principal principal) {

		DeferredResult<APIResponse<ResponseData>> result = new LoggableDeferredResult<>();
		
		eventService.uploadEventTrack(file, eventId, principal.getName())
			.subscribe(m -> {}, 
				       e -> result.setErrorResult(e),
				       () -> result.setResult(ResponseUtils.<ResponseData>createSuccessResponse(new ResponseData())));

		return result;
	}
	
	@RequestMapping(value = "/events/{eventId}/track", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
	@ResponseStatus(OK)
	@ResponseBody
	public DeferredResult<byte[]> getEventTrack(Principal principal, @PathVariable("eventId") Long eventId) {

		DeferredResult<byte[]> result = new LoggableDeferredResult<>();
		
		eventService.getEventTrack(principal.getName(), eventId)
			.subscribe(m -> result.setResult(m), 
				       e -> result.setErrorResult(e));

		return result;
	}

	@RequestMapping(value = "/events/{eventId}/markers", method = RequestMethod.POST)
	@ResponseStatus(CREATED)
	@ResponseBody
	public DeferredResult<APIResponse<ResponseData>> postEvent(@Valid @RequestBody PostMarkerRequest request,
			@PathVariable("eventId") Long eventId, Principal principal) {
		
		DeferredResult<APIResponse<ResponseData>> result = new LoggableDeferredResult<>();
		
		eventService.newMarker(MapUtils.mapToDomain(request), eventId, principal.getName())
				.subscribe(m -> { 
						result.setResult(ResponseUtils.<ResponseData>createSuccessResponse(new ResponseData()));
					},
						   e -> result.setErrorResult(e));
		
		return result;
	}
	
	@RequestMapping(value = "/events/{eventId}/markers", method = RequestMethod.GET)
	@ResponseStatus(OK)
	@ResponseBody
	public DeferredResult<APIResponse<MarkerListResponseData>> getMarkers(@PathVariable("eventId") Long eventId, Principal principal) {
		
		DeferredResult<APIResponse<MarkerListResponseData>> result = new LoggableDeferredResult<>();
		
		eventService.findMarkersByEvent(eventId, principal.getName())
				.subscribe(m -> {
					MarkerListResponseData response = new MarkerListResponseData(MapUtils.mapToMarkerListDto(m));
						result.setResult(ResponseUtils.<MarkerListResponseData>createSuccessResponse(response)); 
					},
						   e -> result.setErrorResult(e));
		
		return result;
	}
}