# Public Sample

This repository serves as a public concept to showcase my coding style and use of Spring framework.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software

```
PostgreSQL 9.3
Postman
```

```
Execute the script located at: /alpha/src/main/resources/initial-data.sql
```

## Running the service
### 1. Run the service
### 2. Get a token
Send the following POST request using Postman
```
POST /oauth/token HTTP/1.1
Host: localhost:8080
Content-Type: application/x-www-form-urlencoded
Authorization: Basic YWxwaGEtd2ViOmFscGhhc2VjcmV0
Cache-Control: no-cache
Postman-Token: 9c9f542d-b698-3fea-eb37-4c256a813087

grant_type=password&username=lfonseca%40gmail.com&password=password
```
You will get the following JSON response back from the service:
```
{
    "access_token": "46080b2b-8fd2-4653-910b-2a885ea2a031",
    "token_type": "bearer",
    "refresh_token": "bb70a108-54a1-4f75-b4c5-72ce53ab059e",
    "expires_in": 42157,
    "scope": "read write delete"
}
```
### 3. Use the API
Send a GET request to fetch the account information based on authenticated user as follows:
```
GET /api/account HTTP/1.1
Host: localhost:8080
Authorization: Bearer 46080b2b-8fd2-4653-910b-2a885ea2a031
Content-Type: application/json
Cache-Control: no-cache
Postman-Token: c90ae2f5-5d0b-ee41-f8f7-672b1597beb7
```
You will get a JSON response back from the service:
```
{
    "success": true,
    "data": {
        "errors": [],
        "username": "lfonseca@gmail.com",
        "firstName": null,
        "lastName": null,
        "email": null,
        "activated": false,
        "authorities": null
    },
    "code": "0000",
    "description": "Success"
}
```

## Built With

* [Spring Framework] - The web framework used
* [Maven]- Dependency Management
* [OAuth2/Spring Security] - Security frameworks

## Authors

* **Luis Carlos Fonseca Acuña** - *Concept*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details